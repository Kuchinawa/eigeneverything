// EigenEverything.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "Math.h"
#include "Matrix.h"

#include <cmath>

#include <iostream>
#include <string>
#include <limits>
#include <vector>

int _tmain(int argc, _TCHAR* argv[])
{
    int choice;
    std::cout << "Choose an example matrix: " << std::endl;
    std::cout << "1: (2x2)" << std::endl;
    std::cout << "2: (3x3, Real, 2 Equal)" << std::endl;
    std::cout << "3: (3x3, Real, All different)" << std::endl;

    while (!((std::cin >> choice) && (choice == 1 || choice == 2 || choice == 3)))
    {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "Please enter a valid choice..." << std::endl;
    }

    std::cout << std::endl;



    Matrix matrixA;

    if (choice == 1)
    {
        matrixA = Matrix(2, 2);

        matrixA.at(0, 0) = 1;
        matrixA.at(0, 1) = 2;

        matrixA.at(1, 0) = 4;
        matrixA.at(1, 1) = 3;
    }
    else if (choice == 2)
    {
        //Real, 2 Equal
        matrixA = Matrix(3, 3);
        matrixA.at(0, 0) = -1;
        matrixA.at(0, 1) = 2;
        matrixA.at(0, 2) = 2;

        matrixA.at(1, 0) = 2;
        matrixA.at(1, 1) = 2;
        matrixA.at(1, 2) = -1;

        matrixA.at(2, 0) = 2;
        matrixA.at(2, 1) = -1;
        matrixA.at(2, 2) = 2;
    }
    else if (choice == 3)
    {
        //Real, all different
        matrixA = Matrix(3, 3);
        matrixA.at(0, 0) = 2;
        matrixA.at(0, 1) = 1;
        matrixA.at(0, 2) = 1;

        matrixA.at(1, 0) = 1;
        matrixA.at(1, 1) = 9;
        matrixA.at(1, 2) = 2;

        matrixA.at(2, 0) = 1;
        matrixA.at(2, 1) = 2;
        matrixA.at(2, 2) = 2;
    }



    std::cout << "Finding eigenvalues of: " << std::endl;
    std::cout << matrixA.toString();

    if (matrixA.rows() == 2)
    {

        //lambda is an eigenvalue of A if det(lambda * idMatrix - matrixA) == 0

        //Create the determinant matrix so we can create the quadratic equation
        Matrix detMatrix(2, 2, false);
        detMatrix -= matrixA;

        float b = detMatrix.get(0, 0) + detMatrix.get(1, 1);
        float c = (detMatrix.get(0, 0) * detMatrix.get(1, 1)) - (detMatrix.get(0, 1) * detMatrix.get(1, 0));

        float x1;
        float x2;

        //Find the eigenvalues by solving the quadratic equation
        Math::solveQuadraticEquation(1.0f, b, c, x1, x2);

        std::cout << "The eigenvalues for the matrix are: " << x1 << " & " << x2 << std::endl;

        std::vector<float> eigenVector1;
        std::vector<float> eigenVector2;

        //Find the eigen vectors
        if (matrixA.get(1, 0) != 0)
        {
            eigenVector1.push_back(x1 - matrixA.get(1, 1));
            eigenVector1.push_back(matrixA.get(1, 0));
            eigenVector2.push_back(x2 - matrixA.get(1, 1));
            eigenVector2.push_back(matrixA.get(1, 0));
        }
        else if (matrixA.get(0, 1) != 0)
        {
            eigenVector1.push_back(matrixA.get(0, 1));
            eigenVector1.push_back(x1 - matrixA.get(0, 0));
            eigenVector2.push_back(matrixA.get(0, 1));
            eigenVector2.push_back(x2 - matrixA.get(0, 0));
        }
        else
        {
            eigenVector1.push_back(1);
            eigenVector1.push_back(0);
            eigenVector2.push_back(0);
            eigenVector2.push_back(1);
        }

        std::cout << "The eigenvectors are:" << std::endl;
        std::cout << "|" << eigenVector1.at(0) << "|" << std::endl;
        std::cout << "|" << eigenVector1.at(1) << "|" << std::endl;
        std::cout << "&" << std::endl;
        std::cout << "|" << eigenVector2.at(0) << "|" << std::endl;
        std::cout << "|" << eigenVector2.at(1) << "|" << std::endl;


    }
    else if (matrixA.rows() == 3)
    {
        float x1, x2, x3;

        Matrix detMatrix(3, 3, false);
        detMatrix -= matrixA;

        //Find the values for our cubic equation (Characteristic polynomial) using the Rule of sarrus and find the unknowns by solving it
        float b = (detMatrix.get(1, 1) + detMatrix.get(2, 2)) + detMatrix.get(0, 0);
        float c = (detMatrix.get(0, 0) * detMatrix.get(2, 2));
        c += detMatrix.get(1, 1) * (detMatrix.get(0, 0) + detMatrix.get(2, 2));
        c -= (detMatrix.get(1, 2) * detMatrix.get(2, 1));
        c -= (detMatrix.get(0, 1) * detMatrix.get(1, 0));
        c -= (detMatrix.get(0, 2) * detMatrix.get(2, 0));
        float d = (detMatrix.get(0, 0) * detMatrix.get(1, 1) * detMatrix.get(2, 2));
        d += (detMatrix.get(0, 1) * detMatrix.get(1, 2) * detMatrix.get(2, 0));
        d += (detMatrix.get(0, 2) * detMatrix.get(1, 0) * detMatrix.get(2, 1));
        d -= (detMatrix.get(0, 2) * detMatrix.get(1, 1) * detMatrix.get(2, 0));
        d -= (detMatrix.get(0, 1) * detMatrix.get(1, 0) * detMatrix.get(2, 2));
        d -= (detMatrix.get(0, 0) * detMatrix.get(1, 2) * detMatrix.get(2, 1));


        Math::solveCubicEquation(1, b, c, d, x1, x2, x3);

        std::cout << "The eigenvalues for the matrix are: " << x1 << " & " << x2 << " & " << x3 << std::endl;

        //Math::SolveCubicEquation(1, -4, -9, 36, x1,x2,x3); //Real, all different
        //Math::SolveCubicEquation(1, -3, -9, 27, x1, x2, x3); //Real, 2 Equal


        //Make (Lambda * Identity - A) matrix
        Matrix lamdaM1 = Matrix(3, 3, true) * x1;
        lamdaM1 -= matrixA;

        Matrix lamdaM2 = Matrix(3, 3, true) * x2;
        lamdaM2 -= matrixA;
        
        Matrix lamdaM3 = Matrix(3, 3, true) * x3;
        lamdaM3 -= matrixA;

        //Reduced row echelon
        Matrix reducedM1 = Math::reducedRowEchelonForm(lamdaM1);
        Matrix reducedM2 = Math::reducedRowEchelonForm(lamdaM2);
        Matrix reducedM3 = Math::reducedRowEchelonForm(lamdaM3);

        //???
    }

    std::cout << "Press ENTER to continue...";
    std::cin.sync();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    return 0;
}
