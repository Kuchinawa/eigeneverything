#include "stdafx.h"

#include "Math.h"

#include "Matrix.h"

#include <stdexcept>

void Math::solveQuadraticEquation(float a, float b, float c, float &x1, float &x2)
{
    if (a == 0)
    {
        throw std::invalid_argument("a cannot be zero.");
    }

    //Determinant
    float D = b * b - 4 * a * c;
    D = squareRoot(D);

    if (D < 0)
    {
        throw std::invalid_argument("Determinant is negative. No solutions.");
    }
    else if (D == 0)
    {
        //Only one solution
        x1 = x2 = -b / 2 * a;
    }
    else
    {   //Two solutions
        x1 = (-b + D) / 2 * a;
        x2 = (-b - D) / 2 * a;
    }
}

float Math::squareRoot(float x)
{
    const float tolerance = 0.000005;

    if (x < 0.0)
    {
        throw std::invalid_argument("sqrt argument cannot be negative.");
    }
    else if (x == 0)
    {
        return 0.0f;
    }
    else
    {
        float oldApprox = x;
        float newApprox = (oldApprox + x / oldApprox) / 2;

        while (absolute((newApprox - oldApprox) / newApprox) > tolerance)
        {
            oldApprox = newApprox;
            newApprox = (oldApprox + x / oldApprox) / 2;
        }

        return newApprox;
    }
}

float Math::determinant(const Matrix &matrix)
{
    if (matrix.columns() == 2)
    {
        //ad-bc method
        return matrix.get(0, 0) * matrix.get(1, 1) - matrix.get(0, 1) * matrix.get(1, 0);
    }
    else
    {
        std::vector<float> resultVector;
        for (int c = 0; c < matrix.columns(); c++)
        {
            //Switch sign based on the column
            int multiplier = matrix.get(0, c) * power(-1, 2 + c);
            
            //Remove the 1st row and column c
            Matrix newMatrix = matrix;
            newMatrix.deleteRow(0);
            newMatrix.deleteColumn(c);

            //get the determinant of the remaining matrix (Recursive)
            float D = determinant(newMatrix);

            resultVector.push_back(multiplier * D);
        }

        //Return the sum of the results
        float determinant = 0;

        for (float &x : resultVector)
        {
            determinant += x;
        }

        return determinant;
    }
}

float Math::power(const float x, const float e)
{
    if (e == 0)
    {
        //x^0 == 0
        return 1.0f;
    }
    else
    {
        float result = x;
        for (int i = 0; i < absolute(e) - 1; i++)
        {
            result *= x;
        }

        if (e >= 0)
        {
            return result;
        }
        else
        {   //x^-y == 1/x^y
            return 1 / result;
        }
    }
}

float Math::absolute(const float x)
{
    return (x >= 0) ? x : -x;
}

//AX^3+BX^2+CX+D=0
void Math::solveCubicEquation(const float A, const float B, const float C, const float D, float &x1, float &x2, float &x3)
{
    if (A == 0.0)
    {
        throw std::invalid_argument("A cannot be 0!");
    }

    float b = B / A;
    float c = C / A;
    float d = D / A;

    float q = (3.0f * c - power(b, 2)) / 9.0f;
    float r = -(27.0f * d) + b*(9.0f * c - 2.0f * power(b, 2));
    r /= 54.0f;

    float discriminant = power(q, 3) + power(r, 2);

    float term1 = (b / 3.0f);

    if (discriminant > 0) //One root real, two complex
    {
        throw std::invalid_argument("Complex roots are not implemented!");
        return;
    }

    if (discriminant == 0) //All roots real, at least two equal
    {
        float r13 = ((r < 0) ? -powf(-r, (1.0f / 3.0f)) : powf(r, (1.0f / 3.0f)));
        x1 = -term1 + 2.0f * r13;
        x2 = x3 = -(r13 + term1);
        return;
    }

    //All roots are real and unequal, q < 0
    q = -q;

    float dum1 = power(q, 3);
    dum1 = acosf(r / squareRoot(dum1));

    float r13 = 2.0f * squareRoot(q);

    x1 = -term1 + r13 * cosf(dum1 / 3.0f);
    x2 = -term1 + r13 * cosf((dum1 + 2.0f * PI) / 3.0f);
    x3 = -term1 + r13 * cosf((dum1 + 4.0f * PI) / 3.0f);

    return;
}

Matrix Math::reducedRowEchelonForm(Matrix m)
{
    int lead = 0;
    int rows = m.rows();
    int columns = m.columns();

    for (int r = 0; r < rows; r++)
    {
        if (columns <= lead)
        {
            return m;
        }

        int i = r;
        while (m.get(i, lead) == 0)
        {
            i++;
            if (i == rows)
            {
                i = r;
                lead++;
                if (columns == lead)
                {
                    lead--;
                    return m;
                }
            }
        }

        for (int j = 0; j < columns; j++)
        {
            float temp = m.get(r, j);
            m.at(r, j) = m.get(i, j);
            m.at(i, j) = temp;
        }

        float div = m.get(r, lead);
        for (int j = 0; j < columns; j++)
        {
            m.at(r, j) /= div;
        }
        for (int j = 0; j < rows; j++)
        {
            if (j != r)
            {
                float sub = m.get(j, lead);
                for (int k = 0; k < columns; k++)
                {
                    m.at(j, k) -= (sub * m.get(r, k));
                }
            }
        }
        lead++;
    }
    return m;

}