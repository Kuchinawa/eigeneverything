#pragma once

#define PI 3.14159265358979323846  /* pi */

class Matrix;

class Math
{
public:
    static void solveQuadraticEquation(float a, float b, float c, float &x1, float &x2);

    static float squareRoot(float x);

    static float determinant(const Matrix &matrix);

    static float power(const float x, const float e);

    static float absolute(const float x);

    static void solveCubicEquation(float A, float B, float C, float D, float &x1, float &x2, float &x3);
    
    static Matrix reducedRowEchelonForm(Matrix m);
};