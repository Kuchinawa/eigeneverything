#pragma once

#include <vector>
#include <string>

class Matrix
{
public:
    Matrix(int row = 2, int column = 2, bool idMatrix = true);
    ~Matrix();

    Matrix(const Matrix& m)
    {
        values = m.values;
    }

    float &at(const int row, const int column);

    float get(const int row, const int column) const;

    int rows() const;
    int columns() const;

    void deleteRow(const int r);
    void deleteColumn(const int c);

    std::string toString();

    Matrix& operator=(const Matrix& m);

    Matrix& operator*(const Matrix& m);

    Matrix& operator*=(const Matrix& m);

    Matrix& operator*(const float& value);

    Matrix& operator+(const Matrix& m);

    Matrix& operator+=(const Matrix& m);

    Matrix& operator-(const Matrix& m);

    Matrix& operator-=(const Matrix& m);

private:
    std::vector<std::vector<float>> values;
};

